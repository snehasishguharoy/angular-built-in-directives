import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { IntroComponent } from './components/intro/intro.component';
import { NgClassExampleComponent } from './components/ng-class-example/ng-class-example.component';
import { NgForExampleComponent } from './components/ng-for-example/ng-for-example.component';
import { NgNonBindableExampleComponent } from './components/ng-non-bindable-example/ng-non-bindable-example.component';
import { NgStyleExampleComponent } from './components/ng-style-example/ng-style-example.component';
import { NgSwitchExampleComponent } from './components/ng-switch-example/ng-switch-example.component';
import { SidebarItemComponent } from './components/sidebar-item/sidebar-item.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { ExampleDef } from './models/ExampleDef';
import { RouterModule, Routes, Router } from '@angular/router';
import { APP_BASE_HREF, LocationStrategy, HashLocationStrategy } from '@angular/common';
import { NgIfComponent } from './components/ng-if/ng-if.component';





export const examples: ExampleDef[] = [
  { label: 'Intro', name: 'Root', path: '', component: 'IntroComponent' },
  { label: 'NgIf', name: 'NgIf', path: 'ng_if', component: 'NgIfComponent' },
  { label: 'NgFor', name: 'NgFor', path: 'ng_for', component: 'NgForExampleComponent' },
  { label: 'NgSwitch', name: 'NgSwitch', path: 'ng_switch', component: 'NgSwitchExampleComponent' },
  { label: 'NgStyle', name: 'NgStyle', path: 'ng_style', component: 'NgStyleExampleComponent' },
  { label: 'NgClass', name: 'NgClass', path: 'ng_class', component: 'NgClassExampleComponent' },
  { label: 'NgNonBindable', name: 'NgNonBindable', path: 'ng_non_bindable', component: 'NgNonBindableExampleComponent' }
];
const routes: Routes = [
  { path: '', component: IntroComponent, pathMatch: 'full' },
  { path: 'ng_if', component: NgIfComponent, pathMatch: 'full' },
  { path: 'ng_for', component: NgForExampleComponent, pathMatch: 'full' },
  { path: 'ng_switch', component: NgSwitchExampleComponent, pathMatch: 'full' },
  { path: 'ng_style', component: NgStyleExampleComponent, pathMatch: 'full' },
  { path: 'ng_class', component: NgClassExampleComponent, pathMatch: 'full' },
  { path: 'ng_non_bindable', component: NgNonBindableExampleComponent, pathMatch: 'full' },
];


@NgModule({
  declarations: [
    AppComponent,
    IntroComponent,
    NgClassExampleComponent,
    NgForExampleComponent,
    NgNonBindableExampleComponent,
    NgStyleExampleComponent,
    NgSwitchExampleComponent,
    SidebarItemComponent,
    SidebarComponent,
    NgIfComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes)
  ],
  providers: [
    { provide: "ExampleDefs", useValue: examples }, {
      provide: "APP_BASE_HREF", useValue: "/app"
    }, {
      provide: LocationStrategy, useClass: HashLocationStrategy
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
