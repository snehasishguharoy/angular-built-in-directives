import { Component, OnInit, Input } from '@angular/core';
import { ExampleDef } from 'src/app/models/ExampleDef';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  @Input() items:ExampleDef[];

  constructor() { }

  ngOnInit() {
  }

}
