import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ng-for-example',
  templateUrl: './ng-for-example.component.html',
  styleUrls: ['./ng-for-example.component.css']
})
export class NgForExampleComponent implements OnInit {
  cities: string[] = ["Kolkata", "Mumbai", "Bangalore", "New Delhi", "Hyderabad"];
  people = [{ name: "Snehasish Guha Roy", age: 21, city: "Kolkata" },
  { name: "Sainadhri Bhattacharya", age: 24, city: "Kolkata" },
  { name: "Samik Roy", age: 32, city: "Kolkata" },
  { name: "Tanmoy Saha", age: 35, city: "Mumbai" }
  ];
  peopleByCity = [{
    city: "Kolkata",
    people: [{ name: "Samik Roy", age: 32 },
    { name: "Snehasish Guha Roy", age: 28 },
    { name: "Sainadhri Bhattacharya", age: 25 }
    ]
  },{
    city:"Mumbai",
    people:[{name:"Sourav Praksh Gupta",age:25},
      {name:"Tanmoy Saha",age:35}
  ]
  },
  {
    city:"New Delhi",
    people:[{
      name:"Avik Roy",
      age:30
    },{
      name:"Hirok Ghosh",
      age:44
    }]
  }
]
  constructor() { }
  ngOnInit() {
  }
}
