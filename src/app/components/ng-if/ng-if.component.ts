import { Component, OnInit } from '@angular/core';
import { noComponentFactoryError } from '@angular/core/src/linker/component_factory_resolver';

@Component({
  selector: 'app-ng-if',
  templateUrl: './ng-if.component.html',
  styleUrls: ['./ng-if.component.css']
})
export class NgIfComponent implements OnInit {
  a:number=56;
  b:number=25;
  str:string="no";

  constructor() { }

  ngOnInit() {
  }
  myFunc():boolean{
    return true;
  }

}
