import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ng-style-example',
  templateUrl: './ng-style-example.component.html',
  styleUrls: ['./ng-style-example.component.css']
})
export class NgStyleExampleComponent implements OnInit {
  color:string;
  font:number;
  style:{
    'background-color': string,
    'border-radius':string,
      border?: string,
      height?: string,
      width?: string
  }
  constructor() { }

  ngOnInit() {
    this.color="green";
    this.font=17;
    this.style={
      'background-color': '#ccc',
      'border-radius': '50px',
      'height': '100px',
      'width': '100px'
    };
   
  }
  apply(color:string,font:number):void{
    this.color=color;
    this.font=font;
  }
  
}
