import { Component, OnInit, Input, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { ExampleDef } from 'src/app/models/ExampleDef';

@Component({
  selector: 'app-sidebar-item',
  templateUrl: './sidebar-item.component.html',
  styleUrls: ['./sidebar-item.component.css']
})
export class SidebarItemComponent implements OnInit {
  @Input() item: ExampleDef

  constructor(private router: Router, private activatedRoute: ActivatedRoute, private location: Location) { }

  ngOnInit() {
  }
  isActive():boolean{
     console.log(`/${this.item.path}`=== this.location.path());
    return `/${this.item.path}`=== this.location.path();
  }

}
